//
//  AppDelegate.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 24.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "AppDelegate.h"
#import <AWSCore/AWSCore.h>
#import <MagicalRecord/MagicalRecord.h>
#import "Constants.h"
#import "TranscodeManager.h"
#import "ApiAWS.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //setup MagicalRecord
    [MagicalRecord setupAutoMigratingCoreDataStack];

    // setup Amazon AWS
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWS_REGION
                                                                                                    identityPoolId:AWS_IDENTITY_POOL_ID];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWS_REGION credentialsProvider:credentialsProvider];
    configuration.timeoutIntervalForRequest = S3_UPLOAD_TIMEOUT_FOR_ONE_TRY;
    configuration.timeoutIntervalForResource = S3_UPLOAD_TIMEOUT_FOR_ONE_TRY;
    configuration.maxRetryCount = S3_UPLOAD_RETRY_COUNT;
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    
    /* -- test
    TranscodeManager *manager = [[TranscodeManager alloc] init];
    [manager transcodeFile:@"B815DABE-0FB4-4512-BDA3-4D7D48877B27.MOV" completionBlock:^(NSString * _Nullable transcodedFileUrl, NSError * _Nullable error) {
        NSLog(@"complete");
    }];
    */
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
