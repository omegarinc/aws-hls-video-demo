//
//  PlayerViewController.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 28.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "PlayerViewController.h"
#import <AVFoundation/AVFoundation.h>

@implementation PlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *videoURL = [NSURL URLWithString:self.videoUrl];
    self.player = [AVPlayer playerWithURL:videoURL];
/*
    AVPlayerItem *item = [[AVPlayerItem alloc] initWithURL:videoURL];
    item.preferredPeakBitRate = 400;
    self.player = [AVPlayer playerWithPlayerItem:item];
 */
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.player play];
}

@end
