//
//  VideoTableViewController.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 25.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "VideoTableViewController.h"
#import "PlayerViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "UploadManager.h"
#import "DataManager.h"
#import "TranscodeManager.h"
#import "Video.h"
#import "Constants.h"

@interface VideoTableViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) NSArray<Video *> *items;
@end

@implementation VideoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(pullToRefresh)
                  forControlEvents:UIControlEventValueChanged];
    [self refreshData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)addButtonTapped:(UIBarButtonItem *)sender {
    [self startMediaBrowserFromViewController:self usingDelegate:self];
}

- (BOOL)startMediaBrowserFromViewController:(UIViewController*) controller
                               usingDelegate:(id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    // Displays only movies
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    
    mediaUI.allowsEditing = NO;
    mediaUI.delegate = delegate;
    [controller presentViewController:mediaUI animated:YES completion:nil];
    
    return YES;
}

#pragma mark - main

- (void)pullToRefresh
{
    [self refreshData];
    [self.refreshControl endRefreshing];
}

- (void)refreshData
{
    self.items = [DataManager videoList];
    [self.tableView reloadData];
}

- (void)processVideoWithUrl:(NSURL *)url
{
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Uploading on AWS S3...";

    __weak typeof(self) weakSelf = self;
    [[UploadManager alloc] uploadFile:[url path] withName:nil thumbnailFile:nil withThumbnailName:nil progressForMainFileBlock:nil completionBlock:^(NSString * _Nullable uploadedFileUrl, NSString * _Nullable uploadedThumbnailUrl, NSError * _Nullable error) {
        typeof(weakSelf) strongSelf = weakSelf;
        
        if (!error) {
            [DataManager saveS3VideoPathToDB:uploadedFileUrl completon:^{
                
                [strongSelf transcodeVideoWithName:uploadedFileUrl completion:^(NSError *error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
                        [strongSelf refreshData];
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Successfully uploaded!" message:@"Wait some time is required for video encoding" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                             {
                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                 
                                             }];
                        [alert addAction:ok];
                        [strongSelf presentViewController:alert animated:YES completion:nil];
                    });
                }];
            }];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                [alert addAction:ok];
                [strongSelf presentViewController:alert animated:YES completion:nil];
            });
        }
    }];
}

- (void)transcodeVideoWithName:(NSString *)videoName completion:(CompletionBlock)completion
{
    TranscodeManager *manager = [[TranscodeManager alloc] init];
    [manager transcodeFile:videoName completionBlock:^(NSError * _Nullable error) {
        completion(error);
        NSLog(@"complete");
    }];
}

- (NSString *)videoUrlWithIndexPath:(NSIndexPath *)indexPath
{
    Video *video = [self.items objectAtIndex:indexPath.row];
    NSString *fileNameWOExt = [video.urlVideo stringByDeletingPathExtension];
    
    NSString *videoUrl = [NSString stringWithFormat:@"%@/%@/index.m3u8", CLOUDFRONT_URL,  fileNameWOExt];
    return  videoUrl;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSURL *videoUrl = nil;
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        videoUrl = (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        NSString *moviePath = [videoUrl path];

        NSLog(@"%@", moviePath);
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self processVideoWithUrl:videoUrl];
    }];
    
    
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const cellReuseIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier forIndexPath:indexPath];
    
    if (!cell) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellReuseIdentifier];
    
    Video *video = [self.items objectAtIndex:indexPath.row];
    cell.textLabel.text = video.urlVideo;
    
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    NSString *videoUrl = [self videoUrlWithIndexPath:indexPath];
    ((PlayerViewController *)segue.destinationViewController).videoUrl = videoUrl;
 
    NSLog(@"videoUrl = %@", videoUrl);
}

@end
