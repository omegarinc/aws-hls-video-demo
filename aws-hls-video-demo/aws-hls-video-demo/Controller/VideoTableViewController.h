//
//  VideoTableViewController.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 25.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoTableViewController : UITableViewController

@end
