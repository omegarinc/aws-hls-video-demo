//
//  PlayerViewController.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 28.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>

@interface PlayerViewController : AVPlayerViewController
@property (nonatomic, strong) NSString *videoUrl;
@end
