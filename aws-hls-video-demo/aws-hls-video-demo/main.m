//
//  main.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 24.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
