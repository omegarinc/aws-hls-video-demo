//
//  NSDate+Extended.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 27.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "NSDate+Extended.h"

@implementation NSDate (Extended)

- (NSString *)sam_ISO8601String {
    struct tm *timeinfo;
    char buffer[80];
    
    time_t rawtime = (time_t)[self timeIntervalSince1970];
    timeinfo = gmtime(&rawtime);
    
    strftime(buffer, 80, "%Y-%m-%dT%H:%M:%SZ", timeinfo);
    
    return [NSString stringWithCString:buffer encoding:NSUTF8StringEncoding];
}

- (NSString *)dateAsYYYYMMDD
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYYMMdd"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];    
    
    return [formatter stringFromDate:self];
}

- (NSString *)dateAsYYYYMMDDTHHMMSSZ
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYYMMdd'T'HHmmss'Z'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    return [formatter stringFromDate:self];
}

@end
