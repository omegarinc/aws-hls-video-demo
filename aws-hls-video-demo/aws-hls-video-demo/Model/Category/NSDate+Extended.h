//
//  NSDate+Extended.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 27.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extended)
- (NSString *)sam_ISO8601String;
- (NSString *)dateAsYYYYMMDD;
- (NSString *)dateAsYYYYMMDDTHHMMSSZ;
@end
