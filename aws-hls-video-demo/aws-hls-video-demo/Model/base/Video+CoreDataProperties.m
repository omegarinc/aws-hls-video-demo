//
//  Video+CoreDataProperties.m
//  
//
//  Created by Alexander Lobanov on 25.03.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Video+CoreDataProperties.h"

@implementation Video (CoreDataProperties)

@dynamic name;
@dynamic urlVideo;
@dynamic urlCoverImage;
@dynamic date;

@end
