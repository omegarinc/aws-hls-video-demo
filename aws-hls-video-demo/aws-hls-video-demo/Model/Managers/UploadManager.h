//
//  UploadManager.h
//  Guitarability
//
//  Created by Maxim Solovyev on 07/11/15.
//  Copyright © 2015 Guitarability. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

typedef void(^UploadMediaFileCompletion)(NSString * _Nullable uploadedFileUrl, NSString  * _Nullable uploadedThumbnailUrl, NSError  * _Nullable error);

@interface UploadManager : NSObject

- (void)uploadFile:(NSString * _Nonnull)filePath
          withName:(NSString * _Nullable)fileName
     thumbnailFile:(NSString * _Nullable)thumbnailFilePath
 withThumbnailName:(NSString * _Nullable)thumbnailFileName
progressForMainFileBlock:(UploadProgressBlock _Nullable)progressBlock
   completionBlock:(UploadMediaFileCompletion _Nonnull)completionBlock;

- (void)cancelUploading;

@end
