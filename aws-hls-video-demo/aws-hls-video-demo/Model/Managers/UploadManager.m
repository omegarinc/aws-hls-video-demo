//
//  UploadManager.m
//  Guitarability
//
//  Created by Maxim Solovyev on 07/11/15.
//  Copyright © 2015 Guitarability. All rights reserved.
//

#import "UploadManager.h"
#import "S3Service.h"
#import <AWSS3/AWSS3.h>

@interface UploadManager ()
@property (strong, nonatomic) AWSS3TransferManagerUploadRequest *currentUploadRequest;
@property (nonatomic) BOOL isUploadingCancelled;
@end

@implementation UploadManager

- (void)uploadFile:(NSString *)filePath
          withName:(NSString *)fileName
     thumbnailFile:(NSString *)thumbnailFilePath
 withThumbnailName:(NSString *)thumbnailFileName
progressForMainFileBlock:(UploadProgressBlock _Nullable)progressBlock
   completionBlock:(UploadMediaFileCompletion)completionBlock
{
    self.isUploadingCancelled = NO;
    
    NSString *fileNameInBucket;
    
    if (! fileName.length) {
        fileNameInBucket = [S3Service generateUniqueFileNameForFileWithPath:filePath];
    } else {
        fileNameInBucket = fileName;
    }
    
    __block NSString *fileUrlInBucket;
    __block NSString *thumbnailUrlInBucket;
    // upload file
    self.currentUploadRequest = [S3Service uploadFile:filePath
                                     fileNameInBucket:fileNameInBucket
                                        progressBlock:progressBlock
                                      completionBlock:^(NSError *error) {
        if (!error) {
            fileUrlInBucket = fileNameInBucket;//[S3Service fileUrlInBucketForFileNameInBucket:fileNameInBucket];

            // upload thumbnail if needed
            if (thumbnailFilePath) {
                
                NSString *thumbnailFileNameInBucket;
                if (! fileName.length) {
                    thumbnailFileNameInBucket = [S3Service generateUniqueFileNameForFileWithPath:thumbnailFilePath];
                } else {
                    thumbnailFileNameInBucket = thumbnailFileName;
                }
                
                self.currentUploadRequest = [S3Service uploadFile:thumbnailFilePath
                                                 fileNameInBucket:thumbnailFileNameInBucket
                                                    progressBlock:nil   // upload without progress
                                                  completionBlock:^(NSError *error) {
                    if (!error) {
                        thumbnailUrlInBucket = [S3Service fileUrlInBucketForFileNameInBucket:thumbnailFileNameInBucket];
                    }
                    if (!self.isUploadingCancelled) {
                        completionBlock(fileUrlInBucket, thumbnailUrlInBucket, error);
                    }
                }];
            } else {
                if (!self.isUploadingCancelled) {
                    completionBlock(fileUrlInBucket, nil, error);
                }
            }
        } else {
            if (!self.isUploadingCancelled) {
                completionBlock(nil, nil, error);
            }
        }
    }];
}


- (void)cancelUploading
{
    self.isUploadingCancelled = YES;
    
    if (self.currentUploadRequest) {
        [self.currentUploadRequest cancel];
        self.currentUploadRequest = nil;
    }
}

@end
