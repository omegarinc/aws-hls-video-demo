//
//  DataManager.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 25.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "DataManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Video.h"

@implementation DataManager

+ (void)saveS3VideoPathToDB:(NSString *)s3VideoPath completon:(VoidBlock)completion
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        Video *video = [Video MR_createEntityInContext:localContext];
        
        video.date = [NSDate date];
        video.urlVideo = s3VideoPath;
        
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        completion();
    }];
}

+ (NSArray <Video *>*)videoList
{
    return [Video MR_findAllSortedBy:@"date" ascending:NO];
}

@end
