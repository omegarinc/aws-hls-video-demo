//
//  DataManager.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 25.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

@class Video;

@interface DataManager : NSObject
+ (void)saveS3VideoPathToDB:(NSString *)s3VideoPath completon:(VoidBlock)completion;
+ (NSArray <Video *>*)videoList;
@end
