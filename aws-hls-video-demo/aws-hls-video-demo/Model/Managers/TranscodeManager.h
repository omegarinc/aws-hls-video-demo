//
//  TranscodeManager.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 25.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

@interface TranscodeManager : NSObject

- (void)transcodeFile:(NSString * _Nonnull)filePath completionBlock:(CompletionBlock _Nonnull)completionBlock;

@end
