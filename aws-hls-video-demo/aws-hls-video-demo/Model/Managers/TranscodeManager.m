//
//  TranscodeManager.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 25.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "TranscodeManager.h"
#import "Constants.h"
#import "ApiAWS.h"

@implementation TranscodeManager

- (void)transcodeFile:(NSString * _Nonnull)fileName completionBlock:(CompletionBlock)completionBlock
{
    ApiAWSCredentials *credentials = [[ApiAWSCredentials alloc] initWithAcceessKey:CREDENTIALS_ACCESS_KEY secretKey:CREDENTIALS_SECRET_ACCESS_KEY region:REGION serviceType:ApiAWSServiceTypeElasticTranscoder];
    ApiAWS *api = [[ApiAWS alloc] initWithCredentials:credentials];
    
    [api createTranscodeJobForFileName:fileName andPipeLine:TRANSCODER_PIPELINE_ID response:^(id responseObject, NSError *error) {
        completionBlock(error);
    }];
}

@end
