//
//  Blocks.h
//  Guitarability
//
//  Created by Вячеслав Ембатуров on 14.09.15.
//  Copyright (c) 2015 Guitarability. All rights reserved.
//

#ifndef Guitarability_Blocks_h
#define Guitarability_Blocks_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//typedef void (^EmptyBlock)();
//typedef void (^ ObjectResultBlock)(id object, NSError *error);
//typedef void (^ ArrayResultBlock)(NSArray *objects, NSError *error);
//typedef void (^ BooleanResultBlock)(BOOL succeeded, NSError *error);
//typedef void (^ IntResultBlock)(NSInteger result, NSError *error);
//typedef void (^ DataResultBlock)(NSData * data, NSError * error);
//typedef void (^ ImageResultBlock)(UIImage * image, NSError * error);
//typedef void (^ NumberResultBlock)(NSNumber *number, NSError *error);
//typedef void (^ URLResultBlock)(NSURL * resultURL, NSError * error);

typedef void (^CompletionBlock) (NSError *error);
typedef void (^ResponseBlock) (id responseObject, NSError *error);
typedef void (^UploadProgressBlock) (int64_t totalBytesSent, int64_t totalBytesExpectedToSend);
typedef void (^VoidBlock)();

#endif
