//
//  ApiAWS.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 28.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

typedef enum : NSUInteger {
    ApiAWSServiceTypeElasticTranscoder,
} ApiAWSServiceType;


@interface ApiAWSCredentials : NSObject
@property (nonatomic, strong) NSString *accessKey;
@property (nonatomic, strong) NSString *secretKey;
@property (nonatomic, strong) NSString *region;
@property (nonatomic, assign) NSString *service;
@property (nonatomic, strong, readonly) NSString *baseUrl;

- (instancetype)initWithAcceessKey:(NSString *)accessKey secretKey:(NSString *)secretKey region:(NSString *)region serviceType:(ApiAWSServiceType)service;

- (NSString *)baseUrlForServiceType:(ApiAWSServiceType)serviceType andRegion:(NSString *)region;

- (void)signRequest:(NSURLRequest *)request;
@end


@interface ApiAWS : NSObject
@property (nonatomic, strong, readonly) ApiAWSCredentials *credentials;

- (instancetype)initWithCredentials:(ApiAWSCredentials *)credentials;

- (void)createTranscodeJobForFileName:(NSString *)filename andPipeLine:(NSString *)pipeLineId response:(ResponseBlock)responseBlock;
@end
