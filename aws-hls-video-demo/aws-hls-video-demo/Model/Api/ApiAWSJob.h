//
//  ApiAWSJob.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 28.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiAWSJobInputDetectedProperties : NSObject
@property (nonatomic, strong) NSString *width;
@property (nonatomic, strong) NSString *height;
@property (nonatomic, strong) NSString *frameRate;
@property (nonatomic, strong) NSString *fileSize;
@property (nonatomic, strong) NSString *durationMillis;

- (NSDictionary *)dictionary;
@end


@interface ApiAWSJobInput : NSObject
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *frameRate;
@property (nonatomic, strong) NSString *resolution;
@property (nonatomic, strong) NSString *aspectRatio;
@property (nonatomic, strong) NSString *interlaced;
@property (nonatomic, strong) NSString *container;
@property (nonatomic, strong) ApiAWSJobInputDetectedProperties *detectedProperties;

- (instancetype)initWithKey:(NSString *)filename;
- (NSDictionary *)dictionary;
@end

@interface ApiAWSJobOutput : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *thumbnailPattern;
@property (nonatomic, strong) NSString *rotate;
@property (nonatomic, strong) NSString *presetId;
@property (nonatomic, strong) NSString *segmentDuration;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *durationMillis;
@property (nonatomic, strong) NSString *width;
@property (nonatomic, strong) NSString *height;
@property (nonatomic, strong) NSString *frameRate;
@property (nonatomic, strong) NSString *fileSize;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *statusDetail;
@property (nonatomic, strong) NSString *appliedColorSpaceConversion;

- (instancetype)initWithKey:(NSString *)key presetId:(NSString *)presetId;
- (NSDictionary *)dictionary;
@end

@interface ApiAWSJobPlaylist : NSObject
@property (nonatomic, strong) NSString *format;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray<NSString *> *outputKeys;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *statusDetail;

- (NSDictionary *)dictionary;
@end


@interface ApiAWSJob : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) ApiAWSJobInput *input;
@property (nonatomic, strong) ApiAWSJobOutput *output;
@property (nonatomic, strong) NSString *outputKeyPrefix;
@property (nonatomic, strong) NSMutableArray<ApiAWSJobOutput *> *outputs;
@property (nonatomic, strong) NSMutableArray<ApiAWSJobPlaylist *> *playlists;
@property (nonatomic, strong) NSString *pipelineId;
@property (nonatomic, strong) NSString *status;

- (NSDictionary *)dictionary;
@end

@interface ApiAWSJobList : NSObject
@property (nonatomic, strong) NSArray<ApiAWSJob *> *jobs;
@property (nonatomic, strong) NSString *nextPageToken;
@end

