//
//  ApiAWS.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 28.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "ApiAWS.h"
#import "ApiAWSJob.h"
#import <AFNetworking/AFNetworking.h>
#import "NSDate+Extended.h"
#import <AWSCore/AWSCore.h>

static NSString *const API_REGION_PARAM = @"#REGION#";
static NSString *const API_SERVICE_PARAM = @"#SERVICE#";
static NSString *const API_BASE_URL = @"https://#SERVICE#.#REGION#.amazonaws.com";

@implementation ApiAWSCredentials

- (instancetype)initWithAcceessKey:(NSString *)accessKey secretKey:(NSString *)secretKey region:(NSString *)region serviceType:(ApiAWSServiceType)serviceType
{
    if (self = [super init]) {
        _accessKey = accessKey;
        _secretKey = secretKey;
        _region = region;
        _service = [self serviceNameForType:serviceType];
        _baseUrl = [self baseUrlForServiceType:serviceType andRegion:region];
    }
    
    return self;
}

- (NSString *)serviceNameForType:(ApiAWSServiceType)serviceType
{
    switch (serviceType) {
        case ApiAWSServiceTypeElasticTranscoder:
            return @"elastictranscoder";
    }
}

- (NSString *)baseUrlForServiceType:(ApiAWSServiceType)serviceType andRegion:(NSString *)region
{
    NSString *service = [self serviceNameForType:serviceType];
    return [[API_BASE_URL stringByReplacingOccurrencesOfString:API_SERVICE_PARAM withString:service] stringByReplacingOccurrencesOfString:API_REGION_PARAM withString:region];
}

- (void)signRequest:(NSMutableURLRequest *)request
{
    NSString *sDateNow = [[NSDate date] dateAsYYYYMMDDTHHMMSSZ];
    NSString *sDateShortNow = [[NSDate date] dateAsYYYYMMDD];
    
    //step 1
    NSString *httpRequestMethod = request.HTTPMethod;
    NSString *canonicalURI = [request.URL path];
    NSString *canonicalQueryString = [request.URL query]? [request.URL query]: @"";
    NSString *canonicalHeaders = [NSString stringWithFormat:@"host:%@\nx-amz-date:%@\n", [request.URL host], sDateNow];
    NSString *signedHeaders = @"host;x-amz-date";
    NSString *payload = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
    NSString *hashRequestPayload = [AWSSignatureSignerUtility hexEncode:[AWSSignatureSignerUtility hashString:payload]];
    
    NSString *canonicalRequest = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@", httpRequestMethod, canonicalURI, canonicalQueryString, canonicalHeaders, signedHeaders, hashRequestPayload];
    
    NSLog(@"canonicalRequest:\n%@\n\n", canonicalRequest);
    
    //step 2
    NSString *credentialScope = [NSString stringWithFormat:@"%@/%@/%@/%@", sDateShortNow, _region, _service, @"aws4_request"];
    NSString *stringToSign = [NSString stringWithFormat:@"%@\n%@\n%@\n%@",
                              AWSSignatureV4Algorithm,
                              sDateNow,
                              credentialScope,
                              [AWSSignatureSignerUtility hexEncode:[AWSSignatureSignerUtility hashString:canonicalRequest]]];
    
    NSLog(@"stringToSign:\n%@\n\n", stringToSign);
    
    //step 3
    //Generate Signature
    NSData *kSigning  = [AWSSignatureV4Signer getV4DerivedKey:_secretKey
                                                         date:sDateShortNow
                                                       region:_region
                                                      service:_service];
    NSData *signatureData = [AWSSignatureSignerUtility sha256HMacWithData:[stringToSign dataUsingEncoding:NSUTF8StringEncoding]
                                                                  withKey:kSigning];
    NSString *signature = [AWSSignatureSignerUtility hexEncode:[[NSString alloc] initWithData:signatureData
                                                                                     encoding:NSASCIIStringEncoding]];
    
    NSLog(@"signature:\n%@\n\n", signature);
    
    //step 4
    NSString *authorization = [NSString stringWithFormat:@"%@ Credential=%@/%@, SignedHeaders=%@, Signature=%@", AWSSignatureV4Algorithm, _accessKey, credentialScope, signedHeaders, signature];
    
    NSLog(@"authorization:\n%@\n\n", authorization);
    
    [request addValue:sDateNow forHTTPHeaderField:@"x-amz-date"];
    [request addValue:authorization forHTTPHeaderField:@"Authorization"];
}

@end


@implementation ApiAWS

- (instancetype)initWithCredentials:(ApiAWSCredentials *)credentials
{
    if (self = [super init]) {
        _credentials = credentials;
    }
    
    return self;
}

- (void)createTranscodeJobForFileName:(NSString *)filename andPipeLine:(NSString *)pipeLineId response:(ResponseBlock)responseBlock
{
    NSString *filenameWOExt = [filename stringByDeletingPathExtension];
    NSDictionary *outputs = @{@"hls_400k_": @"1351620000001-200050",
                              @"hls_600k_": @"1351620000001-200040",
                              @"hls_1M_": @"1351620000001-200030",
                              @"hls_1,5M_": @"1351620000001-200020"};
    
    ApiAWSJob *job = [[ApiAWSJob alloc] init];
    
    ApiAWSJobInput *jobInput = [[ApiAWSJobInput alloc] initWithKey:filename];
    
    job.input = jobInput;
    job.outputKeyPrefix = [NSString stringWithFormat:@"%@/", filenameWOExt];
    job.pipelineId = pipeLineId;
    
    for (NSString *key in [outputs allKeys]) {
        ApiAWSJobOutput *jobOutput = [[ApiAWSJobOutput alloc] initWithKey:key presetId:outputs[key]];
        [job.outputs addObject:jobOutput];
    }
    
    ApiAWSJobPlaylist *jobPlaylist = [[ApiAWSJobPlaylist alloc] init];
    jobPlaylist.outputKeys = [outputs allKeys];
    [job.playlists addObject:jobPlaylist];
    
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:[job dictionary] options:NSJSONWritingPrettyPrinted error:&error];
    NSString *requestBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (!error) {
        NSLog(@"requestBody: %@", requestBody);
        
        [self requestPath:@"/2012-09-25/jobs" httpMethod:@"POST" data:requestBody response:responseBlock];
    } else {
        NSLog(@"error: %@", error.localizedDescription);
    }
}

- (void)requestPath:(NSString *)path httpMethod:(NSString *)method data:(NSString *)data response:(ResponseBlock)responseBlock
{
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", self.credentials.baseUrl, path]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = method;
    request.HTTPBody = [data dataUsingEncoding:NSUTF8StringEncoding];
    
    [self.credentials signRequest:request];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:[request copy] completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@\n\n", error);
            NSLog(@"%@", [[NSString alloc] initWithData:(NSData *)[error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"] encoding:NSUTF8StringEncoding]);
        } else {
            NSLog(@"%@ %@\n\n", response, responseObject);
            responseBlock(responseObject, error);
        }
    }];
    [dataTask resume];
}

@end
