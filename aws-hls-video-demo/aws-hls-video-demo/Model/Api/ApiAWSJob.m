//
//  ApiAWSJob.m
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 28.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#import "ApiAWSJob.h"

#define SAFE_STR(obj) (obj)? obj: @""

@implementation ApiAWSJobInputDetectedProperties

- (NSDictionary *)dictionary
{
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    if (_width) [mDict setObject:SAFE_STR(_width) forKey:@"Width"];
    if (_height) [mDict setObject:SAFE_STR(_height) forKey:@"Height"];
    if (_frameRate) [mDict setObject:SAFE_STR(_frameRate) forKey:@"FrameRate"];
    if (_fileSize) [mDict setObject:SAFE_STR(_fileSize) forKey:@"FileSize"];
    if (_durationMillis) [mDict setObject:SAFE_STR(_durationMillis) forKey:@"DurationMillis"];
    return [mDict copy];
}

@end


@implementation ApiAWSJobInput

- (instancetype)initWithKey:(NSString *)filename
{
    if (self = [super init]) {
        _key = filename;
    }
    
    return self;
}

- (NSDictionary *)dictionary
{
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    if (_key) [mDict setObject:SAFE_STR(_key) forKey:@"Key"];
    if (_frameRate) [mDict setObject:SAFE_STR(_frameRate) forKey:@"FrameRate"];
    if (_resolution) [mDict setObject:SAFE_STR(_resolution) forKey:@"Resolution"];
    if (_aspectRatio) [mDict setObject:SAFE_STR(_aspectRatio) forKey:@"AspectRatio"];
    if (_interlaced) [mDict setObject:SAFE_STR(_interlaced) forKey:@"Interlaced"];
    if (_container) [mDict setObject:SAFE_STR(_container) forKey:@"Container"];
    
    if (_detectedProperties) {
        [mDict setObject:[_detectedProperties dictionary] forKey:@"DetectedProperties"];
    }

    return [mDict copy];
}

@end


@implementation ApiAWSJobOutput

- (instancetype)initWithKey:(NSString *)key presetId:(NSString *)presetId
{
    if (self = [super init]) {
        _key = key;
        _presetId = presetId;
        _segmentDuration = @"10.0";
    }
    
    return self;
}

- (NSDictionary *)dictionary
{
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    if (_id) [mDict setObject:SAFE_STR(_id) forKey:@"Id"];
    if (_key) [mDict setObject:SAFE_STR(_key) forKey:@"Key"];
    if (_thumbnailPattern) [mDict setObject:SAFE_STR(_thumbnailPattern) forKey:@"ThumbnailPattern"];
    if (_rotate) [mDict setObject:SAFE_STR(_rotate) forKey:@"Rotate"];
    if (_presetId) [mDict setObject:SAFE_STR(_presetId) forKey:@"PresetId"];
    if (_segmentDuration) [mDict setObject:SAFE_STR(_segmentDuration) forKey:@"SegmentDuration"];
    if (_duration) [mDict setObject:SAFE_STR(_duration) forKey:@"Duration"];
    if (_durationMillis) [mDict setObject:SAFE_STR(_durationMillis) forKey:@"DurationMillis"];
    if (_width) [mDict setObject:SAFE_STR(_width) forKey:@"Width"];
    if (_height) [mDict setObject:SAFE_STR(_height) forKey:@"Height"];
    if (_frameRate) [mDict setObject:SAFE_STR(_frameRate) forKey:@"FrameRate"];
    if (_fileSize) [mDict setObject:SAFE_STR(_fileSize) forKey:@"FileSize"];
    if (_status) [mDict setObject:SAFE_STR(_status) forKey:@"Status"];
    if (_statusDetail) [mDict setObject:SAFE_STR(_statusDetail) forKey:@"StatusDetail"];
    if (_appliedColorSpaceConversion) [mDict setObject:SAFE_STR(_appliedColorSpaceConversion) forKey:@"AppliedColorSpaceConversion"];
    return [mDict copy];
}

@end


@implementation ApiAWSJobPlaylist

- (instancetype)init
{
    if (self = [super init]) {
        _format = @"HLSv3";
        _name = @"index";
        _outputKeys = [NSArray array];
    }
    
    return self;
}

- (NSDictionary *)dictionary
{
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    if (_format) [mDict setObject:SAFE_STR(_format) forKey:@"Format"];
    if (_name) [mDict setObject:SAFE_STR(_name) forKey:@"Name"];
    if (_status) [mDict setObject:SAFE_STR(_status) forKey:@"Status"];
    if (_statusDetail) [mDict setObject:SAFE_STR(_statusDetail) forKey:@"StatusDetail"];
    
    if (_outputKeys) {
        [mDict setObject:_outputKeys forKey:@"OutputKeys"];
    }
    
    return [mDict copy];
}

@end


@implementation ApiAWSJob

- (instancetype)init
{
    if (self = [super init]) {
        _outputs = [NSMutableArray array];
        _playlists = [NSMutableArray array];
    }
    
    return self;
}

- (NSDictionary *)dictionary
{
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    [mDict setObject:SAFE_STR(_id) forKey:@"Id"];
    if (_input) {
        [mDict setObject:[_input dictionary] forKey:@"Input"];
    }
    if (_output) {
        [mDict setObject:[_output dictionary] forKey:@"Output"];
    }

    [mDict setObject:SAFE_STR(_outputKeyPrefix) forKey:@"OutputKeyPrefix"];
    [mDict setObject:SAFE_STR(_pipelineId) forKey:@"PipelineId"];
    [mDict setObject:SAFE_STR(_status) forKey:@"Status"];
    
    if (_outputs) {
        NSMutableArray *mArr = [NSMutableArray array];
        for (ApiAWSJobOutput *item in _outputs) {
            [mArr addObject:[item dictionary]];
        }
        [mDict setObject:[mArr copy] forKey:@"Outputs"];
    }

    if (_playlists) {
        NSMutableArray *mArr = [NSMutableArray array];
        for (ApiAWSJobPlaylist *item in _playlists) {
            [mArr addObject:[item dictionary]];
        }
        [mDict setObject:[mArr copy] forKey:@"Playlists"];
    }

    return [mDict copy];
}

@end


@implementation ApiAWSJobList

@end