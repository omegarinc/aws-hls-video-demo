//
//  Constants.h
//  aws-hls-video-demo
//
//  Created by Alexander Lobanov on 24.03.16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//AWS

static NSString *const CREDENTIALS_ACCESS_KEY = @"AKIAJXEEUMTO5VXZOHXA";
static NSString *const CREDENTIALS_SECRET_ACCESS_KEY = @"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";

static NSString *const TRANSCODER_PIPELINE_ID = @"1458820928441-0gut0g";
static NSString *const REGION = @"eu-west-1";
static NSString *const SERVICE = @"elastictranscoder";

static NSString *const AWS_IDENTITY_POOL_ID = @"eu-west-1:6e361502-d11d-4282-8f28-a1872db1c3f8";
#define AWS_REGION AWSRegionEUWest1

static NSString *const S3_VIDEO_INBOX_BUCKET = @"hls-video-in";
static NSString *const S3_VIDEO_OUTBOX_BUCKET = @"hls-video-out";

#define S3_VIDEO_INBOX_BUCKET_URL @"https://s3.amazonaws.com/" S3_VIDEO_INBOX_BUCKET @"/"
#define S3_VIDEO_OUTBOX_BUCKET_URL @"https://s3.amazonaws.com/" S3_VIDEO_OUTBOX_BUCKET @"/"

static NSString *const CLOUDFRONT_URL = @"https://d2xrx6iph6ifea.cloudfront.net";

static const NSTimeInterval S3_UPLOAD_TIMEOUT_FOR_ONE_TRY = 30;
static const NSTimeInterval S3_UPLOAD_RETRY_COUNT = 3;

#endif /* Constants_h */
