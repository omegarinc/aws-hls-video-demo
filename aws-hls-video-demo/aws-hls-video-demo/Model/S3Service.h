//
//  S3Service.h
//  Guitarability
//
//  Created by Maxim Solovyev on 29/10/15.
//  Copyright © 2015 Guitarability. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

typedef void(^S3ServiceCompletionBlock) (NSError * _Nullable error);

@class AWSS3TransferManagerUploadRequest;

@interface S3Service : NSObject

+ (AWSS3TransferManagerUploadRequest * _Nullable)uploadFile:(NSString * _Nonnull)filePath
                                           fileNameInBucket:(NSString * _Nullable)fileNameInBucket
                                              progressBlock:(UploadProgressBlock _Nullable)progressBlock
                                            completionBlock:(S3ServiceCompletionBlock _Nonnull)completionBlock;

+ (NSString * _Nullable)generateUniqueFileNameForFileWithPath:(NSString * _Nullable)localFilePath;
+ (NSString * _Nullable)fileUrlInBucketForFileNameInBucket:(NSString * _Nullable)fileNameInBucket;

@end
