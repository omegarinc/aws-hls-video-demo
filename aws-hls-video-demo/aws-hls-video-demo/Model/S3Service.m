//
//  S3Service.m
//  Guitarability
//
//  Created by Maxim Solovyev on 29/10/15.
//  Copyright © 2015 Guitarability. All rights reserved.
//

#import "S3Service.h"
#import <AWSS3/AWSS3.h>
#import "Constants.h"

@implementation S3Service

+ (AWSS3TransferManagerUploadRequest*)uploadFile:(NSString *)filePath
                                fileNameInBucket:(NSString *)fileNameInBucket
                                   progressBlock:(UploadProgressBlock _Nullable)progressBlock
                                 completionBlock:(S3ServiceCompletionBlock _Nonnull)completionBlock
{
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = S3_VIDEO_INBOX_BUCKET;
    uploadRequest.key = fileNameInBucket;
    uploadRequest.body = [NSURL fileURLWithPath:filePath];
    uploadRequest.ACL = AWSS3ObjectCannedACLPublicReadWrite;
    if (progressBlock) {
        uploadRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
            progressBlock(totalBytesSent, totalBytesExpectedToSend);
        };
    }

    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor]
                                                       withBlock:^id(AWSTask *task) {
        if (task.error) {
           if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
               switch (task.error.code) {
                   case AWSS3TransferManagerErrorCancelled:
                   case AWSS3TransferManagerErrorPaused:
                       break;
                       
                   default:
                       NSLog(@"UPLOAD TO S3: Error %@", task.error);
                       break;
               }
           } else {
               // Unknown error.
               NSLog(@"UPLOAD TO S3: Error %@", task.error);
           }
        }
       
        if (task.result) {
            NSLog(@"UPLOAD TO S3: uploaded successfully %@", filePath);
        }
        completionBlock(task.error);
        return nil;
    }];
    return uploadRequest;
}

+ (NSString *)generateUniqueFileNameForFileWithPath:(NSString *)localFilePath
{
    NSString *fileExt = [localFilePath pathExtension];
    NSString *uniqueFileName = [[self generateUniqueFileId] stringByAppendingPathExtension:fileExt];
    return uniqueFileName;
}

+ (NSString *)generateUniqueFileId
{
    return [[NSUUID UUID] UUIDString];
}

+ (NSString *)fileUrlInBucketForFileNameInBucket:(NSString *)fileNameInBucket
{
    return [S3_VIDEO_INBOX_BUCKET stringByAppendingString:fileNameInBucket];
}

@end
