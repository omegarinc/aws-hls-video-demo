# AWS-HLS-VIDEO-DEMO #

### Buckets ###
`hls-video-in` - https://console.aws.amazon.com/s3/home?bucket=hls-video-in&prefix=&region=eu-west-1#

`hls-video-out` - https://console.aws.amazon.com/s3/home?region=eu-west-1#&bucket=hls-video-out&prefix=

### Other AWS Services ###

`aws elastic transcoder` - https://console.aws.amazon.com/elastictranscoder/home?region=eu-west-1#pipelines:

`aws cloudFront` - https://console.aws.amazon.com/cloudfront/home?region=eu-west-1#

`aws cognito` - https://eu-west-1.console.aws.amazon.com/cognito/pool/?id=eu-west-1:6e361502-d11d-4282-8f28-a1872db1c3f8&region=eu-west-1

`aws iam` - https://console.aws.amazon.com/iam/home?region=eu-west-1#users/AlexanderLobanov

This demo use AlexanderLobanov user credentials (access key, secret key)